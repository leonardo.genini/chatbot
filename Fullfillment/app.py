#!/usr/bin/env python

from __future__ import print_function
from future.standard_library import install_aliases
install_aliases()

from urllib.parse import urlparse, urlencode
from urllib.request import urlopen, Request
from urllib.error import HTTPError

import json
import os
import random

from flask import Flask,request, make_response, jsonify

from rdflib import Graph, Literal, BNode, Namespace, RDF, URIRef

# Flask app should start in global layout
app = Flask(__name__)

g = Graph()
g.parse("graph.gotdata", format="turtle")
cbot = Namespace("http://chatbot_bachelor_project_lg.com/")

buttonsPerLine = 3

def chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]

@app.route('/webhook', methods=['POST'])
def webhook():
    req = request.get_json(silent=True, force=True)
    print(req)
    resp = handleReq(req)
    print(str(makeWebhookResult(resp)))
    r = make_response(str(makeWebhookResult(resp)))
    r.headers['Content-Type'] = 'application/json'
    return r

def charButtons(c):
    relations = []
    for s,p,o in g.triples( (c,  None, None) ):
        if not p in relations:
            relations.append(p)
    
    keyboard = []
    if cbot.father in relations:
        keyboard.append('Father')
    if cbot.mother in relations:
        keyboard.append('Mother')
    if cbot.child in relations:
        keyboard.append('Children')
    if cbot.lover in relations:
        keyboard.append('Lover')
    if cbot.allegiance in relations:
        keyboard.append('Allegiance')
    if cbot.origin in relations:
        keyboard.append('Origin')
    if cbot.spouse in relations:
        keyboard.append('Spouse')
    if cbot.sibling in relations:
        keyboard.append('Siblings')
    if cbot.status in relations:
        keyboard.append('Alive?')
    if cbot.age in relations:
        keyboard.append('Age')
    if cbot.descr in relations:
        keyboard.append('Description')
    if cbot.personality in relations:
        keyboard.append('Personality')
    if cbot.img in relations:
        keyboard.append('Picture')
    if cbot.bio in relations:
        keyboard.append('Story')
        
    return list(chunks(keyboard, buttonsPerLine))
    
def placeButtons(p):
    relations = []
    for s,p2,o in g.triples( (p,  None, None) ):
        if not p2 in relations:
            relations.append(p2)
    
    keyboard = []
    if cbot.placeType in relations:
        keyboard.append('Type')
    if cbot.faction in relations:
        keyboard.append('Lords')
    if cbot.descr in relations:
        keyboard.append('Description')
    if cbot.location in relations:
        keyboard.append('Location')
    if cbot.img in relations:
        keyboard.append('Picture')
        
    return list(chunks(keyboard, buttonsPerLine))
    
def factionButtons(f):
    relations = []
    for s,p,o in g.triples( (f,  None, None) ):
        if not p in relations:
            relations.append(p)
    
    keyboard = []
    if cbot.allegiance in relations:
        keyboard.append('Allegiance')
    if cbot.vassal in relations:
        keyboard.append('Vassals')
    if cbot.member in relations:
        keyboard.append('Members')
    if cbot.seat in relations:
        keyboard.append('Seat')
    if cbot.lord in relations:
        keyboard.append('Lord')
    if cbot.descr in relations:
        keyboard.append('Description')
    if cbot.words in relations:
        keyboard.append('Words')
    if cbot.sigil in relations:
        keyboard.append('Sigil')
    if cbot.img in relations:
        keyboard.append('Picture')
        
    return list(chunks(keyboard, buttonsPerLine))
    
def eventButtons(e):
    relations = []
    for s,p,o in g.triples( (e,  None, None) ):
        if not p in relations:
            relations.append(p)
    
    keyboard = []
    if cbot.next in relations:
        keyboard.append('And then?')
    if cbot.descr in relations:
        keyboard.append('Description')
    if cbot.participant in relations:
        keyboard.append('Participants')
    if cbot.casualty in relations:
        keyboard.append('Casualties')
    if cbot.location in relations:
        keyboard.append('Location')
        
    return list(chunks(keyboard, buttonsPerLine))

def handleReq(req):
    result = req.get('result')
    parameters = result.get("parameters")
    action = result.get('action')
    contexts = result.get("contexts")
    print('ACTION :'+action)
    print('PARAMS')
    for par in parameters:
        print(par+" : "+parameters[par])
    print('CONTEXTS')
    for ctxt in contexts:
        print(ctxt)
    if action == 'FindCharRelation':
        return handleFindCharRelation(parameters)
    elif action=='FindFactionRelation':
        return handleFindFactionRelation(parameters)
    elif action=='FindPlaceRelation':
        return handleFindPlaceRelation(parameters)
    elif action=='FindEventRelation':
        return handleFindEventRelation(parameters)
    else:
        return handleGateway(parameters)

def handleGateway(params):
    rel = params.get("Generic_terms")
    txt="I don't understand what you mean"
    contextStr = mkContextString("?", "?")
    img = ''
    if rel == "World":
        txt= "The world of Game of Thrones is vast, here are some a few important locations:"
        keyboardjson = [
        ["Westeros", "Braavos", "Winterfell"],
        ["King's Landing", "Dragonstone", "Meereen"],
      ]          
    elif rel == "Events":
        txt="Here are a few memorable events:"
        keyboardjson = [
        ["Red Wedding", "War of the Five Kings", "Battle of the Bastards"],
        ["Greyjoy Rebellion", "Robert's Rebellion", "Battle of the Blackwater"],
      ]  
    elif rel == "Characters":
        txt="There are many characters, but the most important ones are the following:"
        keyboardjson = [
        ["Jon Snow", "Daenerys Targaryen", "Cersei Lannister"],
        ["Tyrion Lannister", "Sansa Stark", "Euron Greyjoy"],
      ]
    elif rel == "General": 
        txt="Game of Throne is a fantasy TV show based in a medieval setting. Feel free to ask me about its characters, locations or factions to start exploring its universe!"
    elif rel == "Factions":
        txt="Here are some factions that might interrest you:"
        keyboardjson = [
        ["House Stark", "House Lannister", "House Targaryen"],
        ["White Walkers", "Children of the Forest", "House Barratheon"],
      ]
    elif rel == "Random":
        char = random.choice(list(g.subjects(RDF.type, cbot.character)))
        name = str(g.value(subject=char, predicate=cbot.name))
        txt= "Alright, let me see, what would you like to know about "+name+"?"
        contextStr = mkContextString("character", name)
        keyboardjson = charButtons(name)
    else:
        txt = "Hello, I have some knowledge about the Game of Thrones universe. \n\nYou can ask me informations about a specific character, like who his siblings are, where he is from or what his personality or story is. \n\nI also know the various factions, their members and words. And finally I can tell you about the various places and the history of the world"
        keyboardjson = [
        ["Tell me about the characters of Game of Thrones!"],
        ["What do you know about the world?"],
        ["What are the main factions?"],
        ["Can you tell me about some important events?"],    
      ]
    return [txt, contextStr, img, keyboardjson]

def handleFindCharRelation(params):
    txt="I don't understand what you mean"
    char = params.get("Character")
    contextStr = mkContextString("character", char)
    img = ''
    gc = list(g.subjects(cbot.name, Literal(char)))
    if len(gc) == 0:
        txt= "I don't recall any character named "+char+"!"
        return [txt, contextStr, img, []]
    c = gc[0]     
    
    keyboardjson = charButtons(c)
    
    rel = params.get("Relation")
    if rel == "Father":
        fc = list(g.objects(c, cbot.father))
        if len(fc) == 0:
            txt= "I don't think we know anything about "+char+"\'s father"
        else:
            name = str(g.value(subject=fc[0], predicate=cbot.name))
            txt = "If my memory doesn't fail me, "+char+"\'s father is "+name
            #contextStr = mkContextString("character", name)
    elif rel == "Mother":
        fc = list(g.objects(c, cbot.mother))
        if len(fc) == 0:
            txt= "I do not seem to have any record on "+char+"\'s mother"
        else:
            name = str(g.value(subject=fc[0], predicate=cbot.name))
            txt = "I seem to remember "+char+"\'s mother. I think it she was named "+name
            #contextStr = mkContextString("character", name)
    elif rel == "Child":
        fc = list(g.objects(c, cbot.child))
        if len(fc) == 0:
            txt= char+" doesn't have any children!"
        elif len(fc) == 1:
            name = str(g.value(subject=fc[0], predicate=cbot.name))
            txt = char+" only had one child: "+name
            #contextStr = mkContextString("character", name)
        else:
            txt = char+" has "+str(len(fc))+" offsprings: "
            for child in fc[:-2]:
                txt+=str(g.value(subject=child, predicate=cbot.name))+", "
            txt+=str(g.value(subject=fc[-2], predicate=cbot.name))+" and "+str(g.value(subject=fc[-1], predicate=cbot.name))
    elif rel == "Lover":
        fc = list(g.objects(c, cbot.lover))
        if len(fc) == 0:
            txt= "I don't think we know anything about "+char+" love interests"
        else:
            name= str(g.value(subject=fc[0], predicate=cbot.name))
            txt = "I seem to recall a raven carrying a message about, "+char+" interest in "+name
            #contextStr = mkContextString("character", name)
    elif rel == "Faction":
        fc = list(g.objects(c, cbot.allegiance))
        if len(fc) == 0:
            txt= char+" is more or less acting on his own behalf!"
        elif len(fc) == 1:
            name = str(g.value(subject=fc[0], predicate=cbot.name))
            txt = char+" is part of "+name
            #contextStr = mkContextString("faction", name)
        else:
            txt = char+" is a member of the "
            for child in fc[:-2]:
                txt+=str(g.value(subject=child, predicate=cbot.name))+", the "
            txt+=str(g.value(subject=fc[-2], predicate=cbot.name))+" and the "+str(g.value(subject=fc[-1], predicate=cbot.name))
    elif rel == "Origin":
        fc = list(g.objects(c, cbot.origin))
        if len(fc) == 0:
            txt= char+"\'s origins are a mystery"
        else:
            name=str(g.value(subject=fc[0], predicate=cbot.name))
            txt = "I've heard that "+char+" is from "+name
            #contextStr = mkContextString("place", name)
    elif rel == "Spouse":
        fc = list(g.objects(c, cbot.spouse))
        if len(fc) == 0:
            txt= char+" isn't currently married"
        else:
            name=str(g.value(subject=fc[0], predicate=cbot.name))
            txt = char+" and "+name+" are married"
            #contextStr = mkContextString("character", name)
    elif rel == "Siblings" or rel == "Brother" or rel == "Sister":
        fc = list(g.objects(c, cbot.sibling))
        if len(fc) == 0:
            txt= char+" is a single child!"
        elif len(fc) == 1:
            name = str(g.value(subject=fc[0], predicate=cbot.name))
            txt = char+" has one sibling: "+name
            #contextStr = mkContextString("character", name)
        else:
            txt = char+" had "+str(len(fc))+" siblings: "
            for child in fc[:-2]:
                txt+=str(g.value(subject=child, predicate=cbot.name))+", "
            txt+=str(g.value(subject=fc[-2], predicate=cbot.name))+" and "+str(g.value(subject=fc[-1], predicate=cbot.name))
    elif rel == "Status":
        fc = list(g.objects(c, cbot.status))
        if len(fc) == 0:
            txt= "The fate of "+char+" is currently unknown"
        else:
            txt = char+" is currently "+str(fc[0]).lower()+"!"
            if str(fc[0]) == "Dead":
                kb = list(g.objects(c, cbot.killedBy))
                ka = list(g.objects(c, cbot.killedAt))
                if (len(ka)+len(kb)) > 0:
                    txt= char+" died"
                    if len(ka) > 0:
                        txt+= " in "+str(g.value(subject=ka[0], predicate=cbot.name))
                    if len(kb) > 0:
                        txt+= " by the hand of "+str(g.value(subject=kb[0], predicate=cbot.name))
                    txt+="."
    elif rel == "Age":
        fc = list(g.objects(c, cbot.age))
        if len(fc) == 0:
            txt= "I don't know "+char+"'s age..."
        else:
            txt = char+" is "+str(fc[0])+" years old."
    elif rel == "Description":
        fc = list(g.objects(c, cbot.descr))
        if len(fc) == 0:
            txt= "I'm afraid I cannot describe "+char+", my apologies"
        else:
            txt = str(fc[0])
    elif rel == "Personality":
        fc = list(g.objects(c, cbot.personality))
        if len(fc) == 0:
            txt= "I have no clue about "+char+"'s personality"
        else:
            txt = str(fc[0])
    elif rel == "Biography":
        fc = list(g.objects(c, cbot.bio))
        if len(fc) == 0:
            txt= "I don't know anything about "+char+"'s story"
        else:
            txt= str(g.value(subject=URIRef(str(c)+"-story-1"), predicate=cbot.descr))
            contextStr = mkContextString("event", str(URIRef(str(c)+"-story-1")))  
            keyboardjson = eventButtons(URIRef(str(c)+"-story-1"))
    elif rel == "Image":
        fc = list(g.objects(c, cbot.img))
        if len(fc) == 0:
            txt= "Sadly, I don't have any pictures of "+char
        else:
            txt= "Here is a picture of "+char
            img = str(fc[0])
    else:
        txt = "What do you want to know about "+ char+" ?"
    return [txt, contextStr, img, keyboardjson]

def handleFindPlaceRelation(params):
    txt="I don't understand what you mean"
    place = params.get("Place")
    contextStr = mkContextString("place", place)
    img = ''
    gp = list(g.subjects(cbot.name, Literal(place)))
    if len(gp) == 0:
        txt= "I don't recall any place named "+place+"!"
        return [txt, contextStr, img, []]
    p = gp[0]
        
    keyboardjson = placeButtons(p)

    rel = params.get("PlaceRelation")
    if rel == "Description":
        fc = list(g.objects(p, cbot.descr))
        if len(fc) == 0:
            txt= "I couldn't describe "+place+" to you..."
        else:
            txt = str(fc[0])
    elif rel == "Location":
        fc = list(g.objects(p, cbot.location))
        if len(fc) == 0:
            txt= "I'm not quite sure what you mean"
        else:
            name=str(g.value(subject=fc[0], predicate=cbot.name))
            txt = place+" is located in  "+name
            #contextStr = mkContextString("place", name)
    elif rel == "Faction":
        fc = list(g.objects(p, cbot.faction))
        if len(fc) == 0:
            txt= place +" is a free land!"
        else :
            name=str(g.value(subject=fc[0], predicate=cbot.name))
            txt = place+" is currently controlled by "+name
            #contextStr = mkContextString("faction", name)
    elif rel == "Type":
        fc = list(g.objects(p, cbot.placeType))
        if len(fc) == 0:
            txt= "I don't really know what kind of place "+ place+ " is..."
        else:
            txt = place+" is a "+str(fc[0]).lower()
    elif rel == "Image":
        fc = list(g.objects(p, cbot.img))
        if len(fc) == 0:
            txt= "Sadly, I don't have any pictures of "+place
        else:
            txt= "Here is a picture of "+place
            img = str(fc[0])
    else :
        txt = "What do you want to know about "+ place+" ?" 
    return [txt, contextStr, img, keyboardjson]

def handleFindFactionRelation(params):
    txt="I don't understand what you mean"
    faction = params.get("Faction")
    contextStr = mkContextString("faction", faction)
    img=''
    gf = list(g.subjects(cbot.name, Literal(faction)))
    if len(gf) == 0:
        txt= "I don't recall any faction named "+faction+"!"
        return [txt, contextStr, img, []]
    f = gf[0]
        
    keyboardjson = factionButtons(f)
    
    rel = params.get("FactionRelation")
    if rel == "Vassal":
        fc = list(g.objects(f, cbot.vassal))
        if len(fc) == 0:
            txt= faction+" do not seem to have any vassals"
        elif len(fc) == 1:
            name=str(g.value(subject=fc[0], predicate=cbot.name))
            txt = faction+" has one vassal: "+name
            #contextStr = mkContextString("faction", name)
        else:
            txt = faction+" has "+str(len(fc))+" vassals: "
            for vassal in fc[:-2]:
                txt+=str(g.value(subject=vassal, predicate=cbot.name))+", "
            txt+=str(g.value(subject=fc[-2], predicate=cbot.name))+" and "+str(g.value(subject=fc[-1], predicate=cbot.name))
    elif rel == "Member":
        fc = list(g.objects(f, cbot.member))
        if len(fc) == 0:
            txt= "I don't know any particular member of "+ faction
        elif len(fc) == 1:
            name=str(g.value(subject=fc[0], predicate=cbot.name))
            txt = name+" is a known member of "+faction
            #contextStr = mkContextString("character", name)
        else:
            txt = faction+" has several members: "
            for member in fc[:-2]:
                txt+=str(g.value(subject=member, predicate=cbot.name))+", "
            txt+=str(g.value(subject=fc[-2], predicate=cbot.name))+" and "+str(g.value(subject=fc[-1], predicate=cbot.name))+" are all part of "+faction
    elif rel == "Seat":
        fc = list(g.objects(f, cbot.seat))
        if len(fc) == 0:
            txt= faction +" do not really have specific headquarters"
        else :
            name=str(g.value(subject=fc[0], predicate=cbot.name))
            txt = faction+" seat of power is "+name
            #contextStr = mkContextString("place", name)
    elif rel == "Lord":
        fc = list(g.objects(f, cbot.lord))
        if len(fc) == 0:
            txt= "I don't have no clue if "+ faction+ " has a leader"
        else:
            name=str(g.value(subject=fc[0], predicate=cbot.name))
            txt = "The current leader of "+faction+" is "+name
            #contextStr = mkContextString("character", name)
    elif rel == "Description":
        fc = list(g.objects(f, cbot.descr))
        if len(fc) == 0:
            txt= "I'm afraid I can't help you with that"
        else:
            txt = str(fc[0])        
    elif rel== "Words":
        fc = list(g.objects(f, cbot.words))
        if len(fc) == 0:
            txt= faction+ " do not have words!"
        else:
            txt = "Their words are \""+ "".join(fc[0])+"\""    
    elif rel== "Sigil":
        fc = list(g.objects(f, cbot.sigil))
        if len(fc) == 0:
            txt= faction+ " do not seem to have a particular banner"
        else:
            txt = "Their sigil consists of "+ "".join(fc[0])
    elif rel== "Allegiance":
        fc = list(g.objects(f, cbot.allegiance))
        if len(fc) == 0:
            txt= faction+ " fend for themselves"
        else:
            name=str(g.value(subject=fc[0], predicate=cbot.name))
            txt = faction + " is under the authority of "+ name  
            #contextStr = mkContextString("character", name)
    elif rel == "Image":
        fc = list(g.objects(f, cbot.img))
        if len(fc) == 0:
            txt= "Sadly, I don't have any pictures for "+faction
        else:
            txt= "Here is a picture of "+faction
            img = str(fc[0])
    else:
        txt = "What do you want to know about "+ faction+" ?" 
    return [txt, contextStr, img, keyboardjson]

def handleFindEventRelation(params):
    txt="I don't understand what you mean"
    evt = params.get("Event")
    contextStr = mkContextString("event", evt)
    img = ''
    
    ge2 = list(g.subjects(cbot.name, Literal(evt)))
    ge3 = [x for x in ge2 if isinstance(x, Literal)]
    
    if len(ge3) > 0:
        e = ge3[0]
    else:
        ge = list(g.objects(URIRef(evt), cbot.name))
        if len(ge) > 0:
            e = URIRef(evt)
        else:
            txt= "I'm not sure what you're talking about..."
            return [txt, contextStr, img, []]        
        
    print("FOUND EVENT: "+e)
    keyboardjson = eventButtons(e)
    
    rel = params.get("EventRelation")
    if rel == "Description":
        fc = list(g.objects(e, cbot.descr))
        if len(fc) == 0:
            txt= "I seem to have forgotten"
        else:
            txt = str(fc[0])      
    elif rel== "Victim":
        fc = list(g.objects(e, cbot.casualty))
        if len(fc) == 0:
            txt= "I don't think anyone died then"
        elif len(fc) == 1:
            txt = str(g.value(subject=fc[0], predicate=cbot.name))+" died during the "+evt
        else:
            txt = "There were lots of casualties during the "+evt+" :"
            for casu in fc[:-2]:
                txt+=str(g.value(subject=casu, predicate=cbot.name))+", "
            txt+=str(g.value(subject=fc[-2], predicate=cbot.name))+" and "+str(g.value(subject=fc[-1], predicate=cbot.name))   
    elif rel== "Participant":
        fc = list(g.objects(e, cbot.participant))
        if len(fc) == 0:
            txt= "I'm not quite certain, sorry"
        elif len(fc) == 1:
            txt = str(g.value(subject=fc[0], predicate=cbot.name)+" took part in the "+evt)
        else:
            txt = ""
            for part in fc[:-2]:
                txt+=str(g.value(subject=part, predicate=cbot.name))+", "
            txt+=str(g.value(subject=fc[-2], predicate=cbot.name))+" and "+str(g.value(subject=fc[-1], predicate=cbot.name))+" were involved in the "+evt  
    elif rel== "Location":
        fc = list(g.objects(e, cbot.location))
        if len(fc) == 0:
            txt= "I'm not sure where it took place"
        else:
            name = str(g.value(subject=fc[0], predicate=cbot.name))
            txt = "It happened in "+ name
            contextStr = mkContextString("place", name)
    elif rel== "Next":
        fc = list(g.objects(e, cbot.next))
        if len(fc) == 0:
            txt= "I don't remember what happened after"
        else:
            contextStr = mkContextString("event", str(fc[0]))
            print('NEW EVENT CONTEXT')
            print(str(fc[0]))
            txt = str(g.value(subject=fc[0], predicate=cbot.descr))
        
    print('RETURN CONTEXT')
    print(contextStr)
    return [txt, contextStr, img, keyboardjson]

def makeWebhookResult(res):
    if res[2]:
        return makeImgResult(res)
    else:
        return makeTxtResult(res)
 
def makeImgResult(res):
    return {
      'contextOut' : res[1] ,
      'data':{
        'telegram': {
        'method': 'sendPhoto',
        'caption': str(res[0]),
        'photo': res[2],
        'reply_markup': {
        'keyboard': res[3],
        'one_time_keyboard': True,
        'resize_keyboard': True
        }
     }
      }
   }

def makeTxtResult(res):
    return {
      'contextOut' : res[1] ,
      'data':{
        'telegram': {
        'text': str(res[0]),
        'reply_markup': {
        'keyboard': res[3],
        'one_time_keyboard': True,
        'resize_keyboard': True
        }
     }
      }
   }


def mkContextString(type, value):
    if type == "event":
        return [{"name":"eventrel","lifespan":3, "parameters":{"Event":value}}]
    elif type == "character":
        return [{"name":"charrel","lifespan":3, "parameters":{"Character":value}}]
    elif type == "place":
        return [{"name":"placerel","lifespan":3, "parameters":{"Place":value}}]
    elif type == "faction":
        return [{"name":"factionrel","lifespan":3, "parameters":{"Faction":value}}]
    else:
        return [{"name":"unknown","lifespan":0, "parameters":{"Unknown":value}}]
    

if __name__ == '__main__':
    port = int(os.getenv('PORT', 5000))

    print("Starting app on port %d" % port)

    app.run(debug=True, port=port, host='0.0.0.0')

    
