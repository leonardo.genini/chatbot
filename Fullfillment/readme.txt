This folder contains the business logic, or fulfillment, of the chatbot. it is the code that is hosted on Heroku.

files of interest are:
app.py: the actual implementation of the fulfillment.
graph.gotdata: the populated ontology about Game of Thrones (created using the json2rdf.py script)
