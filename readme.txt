This folder contains all the material produced for the bachelor project "The Future of Mobile Apps: the Chatbot".

The subfolders are as follow:
GoTSpider contains the web scraping code
Json2Rdf contains the code used to process the scraped data
Fulfillment: contains the business logic of the chatbot

Each of the folders above also contains a readme.txt for more information.

GotChatbot.zip is an export of the API.ai chatbot agent

The docs folder contains the documentation of this project, and has the following arborescence:

docs
 |--Planning: Containing the planning and specifications
 |--PVs: containing the minutes of the weekly meetings
 |--Report: containing the final report of the project
 |--Flyer: containing the flyer (in french)
 |--Poster: containing the bachelor project exposition poster (in french)
 |--Slides: containing the project defense slides (in french)
 |--Video: containing two versions of a short video presenting the project (with and without music)
