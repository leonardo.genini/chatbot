breadability==0.1.20
certifi==2017.4.17
chardet==3.0.4
docopt==0.6.2
idna==2.5
isodate==0.5.4
lxml==3.8.0
nltk==3.2.4
numpy==1.13.1
pyparsing==2.2.0
rdflib==4.2.2
requests==2.18.1
six==1.10.0
sumy==0.6.0
urllib3==1.21.1

