import json
import os
import re
import requests

with open(os.path.join('./GoTJson', 'GoTcharacter.json')) as cdata_file:
	cdata = json.load(cdata_file)

with open(os.path.join('./GoTJson', 'GoThouse.json')) as fdata_file:
	fdata = json.load(fdata_file)

with open(os.path.join('./GoTJson', 'GoTplace.json')) as pdata_file:
	pdata = json.load(pdata_file)

with open(os.path.join('./GoTJson', 'GoTevent.json')) as edata_file:
	edata = json.load(edata_file)

devToken = "e12d9bf98bf14d49b3876ac0ca19f093"
eventID = "1619c82b-e7d3-478f-82ff-750d94ba20b7"
charID = "2691069e-9404-4d67-9354-780a8e0d0b25"
placeID = "ee82e7fe-991e-4e8e-8d28-7100758146db"
factionID = "a2f308dd-a37b-4195-a268-d5bdc680ffe0"

entries = []
count = 0

for c in cdata:
	count+=1
	name =  re.sub('\([^\)]+\)', '', c["name"])
	
	char = {}

	firstname = name.split()[0]
	synonyms = [name, name.lower(), firstname, firstname.lower()]
	
	if "aka" in c:
		for a in c["aka"]:
			synonyms.append(a)
			synonyms.append(a.lower())
							
	char["synonyms"] = synonyms
	char["value"] = name
	entries.append(char)
	
	if count >= 100:
		data = json.dumps(entries)
		url = 'https://api.api.ai/v1/entities/'+charID+'/entries?v=20150910'
		headers = {'Content-Type':'application/json', 'Authorization':'Bearer '+devToken}
		r = requests.put(url, data=data, headers=headers)
		entries = []	
		count = 0

if len(entries) != 0:
		data = json.dumps(entries)
		url = 'https://api.api.ai/v1/entities/'+charID+'/entries?v=20150910'
		headers = {'Content-Type':'application/json', 'Authorization':'Bearer '+devToken}
		r = requests.put(url, data=data, headers=headers)
		entries = []	
		count = 0
		
for f in fdata:
	count+=1
	name =  re.sub('\([^\)]+\)', '', f["name"])
	
	fac = {}

	synonyms = [name, name.lower()]
	
	if name.startswith("House"):
		if(name.split()[1] != "of"):
			synonyms.append(name.split()[1])
			synonyms.append((name.split()[1]).lower())
		else:
			synonyms.append(name.split()[2])
			synonyms.append((name.split()[2]).lower())
	fac["synonyms"] = synonyms
	fac["value"] = name
	entries.append(fac)
	
	if count >= 100:
		data = json.dumps(entries)
		url = 'https://api.api.ai/v1/entities/'+factionID+'/entries?v=20150910'
		headers = {'Content-Type':'application/json', 'Authorization':'Bearer '+devToken}
		r = requests.put(url, data=data, headers=headers)
		entries = []	
		count = 0

if len(entries) != 0:
		data = json.dumps(entries)
		url = 'https://api.api.ai/v1/entities/'+factionID+'/entries?v=20150910'
		headers = {'Content-Type':'application/json', 'Authorization':'Bearer '+devToken}
		r = requests.put(url, data=data, headers=headers)
		entries = []	
		count = 0
		
for p in pdata:
	count+=1
	name =  re.sub('\([^\)]+\)', '', p["name"])
	
	place = {}

	synonyms = [name, name.lower()]
	
	place["synonyms"] = synonyms
	place["value"] = name
	entries.append(place)
	
	if count >= 100:
		data = json.dumps(entries)
		url = 'https://api.api.ai/v1/entities/'+placeID+'/entries?v=20150910'
		headers = {'Content-Type':'application/json', 'Authorization':'Bearer '+devToken}
		r = requests.put(url, data=data, headers=headers)
		entries = []	
		count = 0

if len(entries) != 0:
		data = json.dumps(entries)
		url = 'https://api.api.ai/v1/entities/'+placeID+'/entries?v=20150910'
		headers = {'Content-Type':'application/json', 'Authorization':'Bearer '+devToken}
		r = requests.put(url, data=data, headers=headers)
		entries = []	
		count = 0
		
for e in edata:
	count+=1
	name =  re.sub('\([^\)]+\)', '', e["name"])
	
	evt = {}

	synonyms = [name, name.lower()]
	
	evt["synonyms"] = synonyms
	evt["value"] = name
	entries.append(evt)
	
	if count >= 100:
		data = json.dumps(entries)
		url = 'https://api.api.ai/v1/entities/'+eventID+'/entries?v=20150910'
		headers = {'Content-Type':'application/json', 'Authorization':'Bearer '+devToken}
		r = requests.put(url, data=data, headers=headers)
		entries = []	
		count = 0

if len(entries) != 0:
		data = json.dumps(entries)
		url = 'https://api.api.ai/v1/entities/'+eventID+'/entries?v=20150910'
		headers = {'Content-Type':'application/json', 'Authorization':'Bearer '+devToken}
		r = requests.put(url, data=data, headers=headers)
		entries = []	
		count = 0