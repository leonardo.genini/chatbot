import logging
import json
import os
from rdflib import Graph, Literal, BNode, Namespace, RDF, URIRef
from rdflib.namespace import DC, FOAF
from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer
from sumy.summarizers.lsa import LsaSummarizer as Summarizer
from sumy.nlp.stemmers import Stemmer
from sumy.utils import get_stop_words
import re

logging.basicConfig()

n = "http://gameofthrones.wikia.com"
cbot = Namespace("http://chatbot_bachelor_project_lg.com/")

LANGUAGE = "english"
DESCRIPTION_SENTENCES_COUNT = 2
EVENT_DESCR_SENTENCES_COUNT = 3
PERSONALITY_SENTENCES_COUNT = 3
stemmer = Stemmer(LANGUAGE)
summarizer = Summarizer(stemmer)
summarizer.stop_words = get_stop_words(LANGUAGE)

def mkGraph():
	with open(os.path.join('./GoTJson', 'GoTcharacter.json')) as cdata_file:
		cdata = json.load(cdata_file)

	with open(os.path.join('./GoTJson', 'GoThouse.json')) as fdata_file:
		fdata = json.load(fdata_file)

	with open(os.path.join('./GoTJson', 'GoTplace.json')) as pdata_file:
		pdata = json.load(pdata_file)

	with open(os.path.join('./GoTJson', 'GoTevent.json')) as edata_file:
		edata = json.load(edata_file)

	chars = []
	factions = []
	places = []
	events = []
	eventsNb = []

	missedUrl = []


	g = Graph()


	for c in cdata:
		nc = URIRef(c["url"])
		chars.append(str(nc))
		g.add( (nc, cbot.name, Literal(re.sub('(\([^\)]+\))|(\[[^\]]+\])', '', c["name"]))) )
		g.add( (nc, RDF.type, cbot.character) )

	for f in fdata:
		nf = URIRef(f["url"])
		factions.append(str(nf))
		g.add( (nf, cbot.name,  Literal(re.sub('(\([^\)]+\))|(\[[^\]]+\])', '', f["name"]))) )
		g.add( (nf, RDF.type, cbot.faction) )

	for p in pdata:
		np = URIRef(p["url"])
		places.append(str(np))
		g.add( (np, cbot.name,  Literal(re.sub('(\([^\)]+\))|(\[[^\]]+\])', '', p["name"]))) )
		g.add( (np, RDF.type, cbot.place) )

	for e in edata:
		ne = URIRef(e["url"])
		events.append(str(ne))
		parser = PlaintextParser.from_string(e["shortDescr"], Tokenizer(LANGUAGE))
		g.add( (ne, cbot.descr, Literal(" ".join([str(x) for x in summarizer(parser.document, EVENT_DESCR_SENTENCES_COUNT)]))) )

		g.add( (ne, RDF.type, cbot.event) )
		if "name" in e:
			g.add( (ne, cbot.name,  Literal(re.sub('(\([^\)]+\))|(\[[^\]]+\])', '', e["name"]))) )
		if "previous" in e:
			g.add( (URIRef(e["previous"]), cbot.next, ne) )
			if e["number"] >1 and e["eventType"] == "Story":
				g.add( (Literal(e["number"]-1), cbot.next, Literal(e["number"])))


	for c in cdata:
		cc = URIRef(c["url"])
		if "appearances" in c:
			g.add( (cc, cbot.appearances,  Literal(c["appearances"])) )
		else:
			g.add( (cc, cbot.appearances,  Literal(0)) )
		g.add( (cc, cbot.status,  Literal(c["status"])) )
		if "shortDescr" in c:
			parser = PlaintextParser.from_string(c["shortDescr"], Tokenizer(LANGUAGE))
			g.add( (cc, cbot.descr, Literal(" ".join([str(x) for x in summarizer(parser.document, DESCRIPTION_SENTENCES_COUNT)]))) )

		if "personality" in c:
			parser = PlaintextParser.from_string(c["personality"], Tokenizer(LANGUAGE))
			g.add( (cc, cbot.personality, Literal(" ".join([str(x) for x in summarizer(parser.document, PERSONALITY_SENTENCES_COUNT)]))) ) 
			
		if "biography" in c:
			storycount = 1
			stories = []
			sp = URIRef(c["url"]+"-story-"+str(storycount))
			parser = PlaintextParser.from_string(c["biography"][0], Tokenizer(LANGUAGE))
			g.add( (cc, cbot.bio, sp) )
			g.add( (sp, cbot.descr, Literal(" ".join([str(x) for x in summarizer(parser.document, EVENT_DESCR_SENTENCES_COUNT)]))) ) 
			g.add( (sp, RDF.type, cbot.bio))
			g.add( (sp, cbot.name, Literal(c["name"]+"-story-"+str(storycount))))
			stories.append(sp)
		
			
			for evt in c["biography"][1:]:
				storycount += 1
				sp = URIRef(c["url"]+"-story-"+str(storycount))
				parser = PlaintextParser.from_string(evt, Tokenizer(LANGUAGE))
				
				g.add( (cc, cbot.bio, sp) )
				g.add( (sp, cbot.descr, Literal(" ".join([str(x) for x in summarizer(parser.document, EVENT_DESCR_SENTENCES_COUNT)]))) )
				g.add( (sp, RDF.type, cbot.bio))
				g.add( (sp, cbot.name, Literal(c["name"]+"-story-"+str(storycount))))
				g.add( (sp, cbot.previous, stories[-1]) )
				g.add( (stories[-1], cbot.next, sp) )
				stories.append(sp)
		
		if "images" in c:
			if len(c["images"]) > 0:
				g.add( (cc, cbot.img, URIRef(c["images"][0]["url"])) )
				
		if "aka" in c:
			for aka in c["aka"]:
				g.add( (cc, cbot.aka, Literal(aka)) )
		if "age" in c:
			g.add( (cc, cbot.age,  Literal(c["age"])) )
		if "deathRefs" in c:
			for url in c["deathRefs"]:
				if n+url in chars:
					g.add( (cc, cbot.killedBy,  URIRef(n+url)) )
				if n+url in places:
					g.add( (cc, cbot.killedAt,  URIRef(n+url)) )
				if n+url in events:
					g.add( (cc, cbot.killedDuring,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "originRefs" in c:
			for url in c["originRefs"]:
				if n+url in places:
					g.add( (cc, cbot.origin,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "allegianceRefs" in c:
			for url in c["allegianceRefs"]:
				if n+url in factions:
					g.add( (cc, cbot.allegiance,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "fatherRefs" in c:
			for url in c["fatherRefs"]:
				if n+url in chars:
					g.add( (cc, cbot.father,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "motherRefs" in c:
			for url in c["motherRefs"]:
				if n+url in chars:
					g.add( (cc, cbot.mother,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "spouseRefs" in c:
			for url in c["spouseRefs"]:
				if n+url in chars:
					g.add( (cc, cbot.spouse,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "childrenRefs" in c:
			for url in c["childrenRefs"]:
				if n+url in chars:
					g.add( (cc, cbot.child,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "siblingsRefs" in c:
			for url in c["siblingsRefs"]:
				if n+url in chars:
					g.add( (cc, cbot.sibling,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "loversRefs" in c:
			for url in c["loversRefs"]:
				if n+url in chars:
					g.add( (cc, cbot.lover,  URIRef(n+url)) )
				else:
					missedUrl.append(url)

		#name, importance=0, titles=[], status="Alive", age=-1, descr=[], bio=[], personality=[], relations=[], links={}
		#KilledDuring, KilledAt, KilledBy, Origin, Allegiance, Father, Mother, Spouse, Child, Sibling, Lover

	for f in fdata:
		ff = URIRef(f["url"])
		if "sigil" in f:
			g.add( (ff, cbot.sigil,  Literal(f["sigil"])) )
		if "words" in f:
			g.add( (ff, cbot.words,  Literal(f["words"])) )
		if "seatRefs" in f:
			for url in f["seatRefs"]:
				if n+url in places:
					g.add( (ff, cbot.seat,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "lordRef" in f:
			for url in f["lordRef"]:
				if n+url in chars:
					g.add( (ff, cbot.lord,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "allegianceRef" in f:
			for url in f["allegianceRef"]:
				if n+url in factions:
					g.add( (ff, cbot.allegiance,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "vassalsRefs" in f:
			for url in f["vassalsRefs"]:
				if n+url in factions:
					g.add( (ff, cbot.vassal,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "membersRefs" in f:
			for url in f["membersRefs"]:
				if n+url in chars:
					g.add( (ff, cbot.member,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "images" in f:
			if len(f["images"]) > 0:
				g.add( (ff, cbot.img, URIRef(f["images"][0]["url"])) )
		if "shortDescr" in f:
			parser = PlaintextParser.from_string(f["shortDescr"], Tokenizer(LANGUAGE))
			g.add( (ff, cbot.descr, Literal(re.sub('(\([^\)]+\))|(\[[^\]]+\])', '', " ".join([str(x) for x in summarizer(parser.document, PERSONALITY_SENTENCES_COUNT)])))))

		#sigil=None, words=None, descr=[], links={}
		#Seat, Lord, Allegiance, Vassal, Member

	for p in pdata:
		pp = URIRef(p["url"])
		if "placeType" in p:
			g.add( (pp, cbot.placeType,  Literal(p["placeType"])) )
		if "houseRef" in p:
			for url in p["houseRef"]:
				if n+url in factions:
					g.add( (pp, cbot.faction,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "location" in p:
			for url in p["location"]:
				if n+url in places:
					g.add( (pp, cbot.location,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "images" in p:
			if len(p["images"]) > 0:
				g.add( (pp, cbot.img, URIRef(p["images"][0]["url"])) )
		if "shortDescr" in p:
			parser = PlaintextParser.from_string(p["shortDescr"], Tokenizer(LANGUAGE))
			g.add( (pp, cbot.descr, Literal(re.sub('(\([^\)]+\))|(\[[^\]]+\])', '', " ".join([str(x) for x in summarizer(parser.document, DESCRIPTION_SENTENCES_COUNT)])))) )

		#name, placeType, descr=[], links={}
		#Faction, Location

	for e in edata:
		ee = Literal(e["number"])
		if "name" in e:
			g.add( (ee, cbot.name,  Literal(e["name"])) )
		if "eventType" in e:
			g.add( (ee, cbot.eventType,  Literal(e["eventType"])) )
		if "year" in e:
			g.add( (ee, cbot.year,  Literal(e["year"])) )
		if "shortDescr" in e:
			g.add( (ee, cbot.descr,  Literal(re.sub('(\([^\)]+\))|(\[[^\]]+\])', '', "".join(e["shortDescr"])))))	
		if "references" in e:
			for url in e["references"]:
				if n+url in chars:
					g.add( (ee, cbot.participant,  URIRef(n+url)) )
				if n+url in places:
					g.add( (ee, cbot.location,  URIRef(n+url)) )
				if n+url in factions:
					g.add( (ee, cbot.faction,  URIRef(n+url)) )
				else:
					missedUrl.append(url)

		if "characterRefs" in e:
			for url in e["characterRefs"]:
				if n+url in chars:
					g.add( (ee, cbot.participant,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "houseRefs" in e:
			for url in e["houseRefs"]:
				if n+url in factions:
					g.add( (ee, cbot.faction,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "placeRefs" in e:
			for url in e["placeRefs"]:
				if n+url in places:
					g.add( (ee, cbot.location,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "casualtiesRefs" in e:
			for url in e["casualtiesRefs"]:
				if n+url in chars:
					g.add( (ee, cbot.casualty,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "battleRefs" in e:
			for url in e["battleRefs"]:
				if n+url in events:
					g.add( (ee, cbot.battle,  URIRef(n+url)) )
				else:
					missedUrl.append(url)
		if "images" in e:
			if len(e["images"]) > 0:
				g.add( (ee, cbot.img, URIRef(e["images"][0]["path"])) )
		if "previous" in e:
			g.add( (ee, cbot.previous, URIRef(e["previous"])) )
			g.add( (URIRef(e["previous"]), cbot.next, ee) )
		#id, name=None, eventType="Story", year=None, descr=[], links={}
		#Character, Faction, Place, Battle, Casualty

	return g

graph = mkGraph()
graph.serialize(destination='graph.gotdata', format='turtle')
