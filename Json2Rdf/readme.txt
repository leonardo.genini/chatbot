This folder contains the code used to manipulate the Json files created by the spider.

Library requirements are in requirements.txt

files of interest are:
GoTJson folder contains the json files to use
json2rdf.py: a script used to generate a graph, thanks to RDFLib, from the json files
graph.gotdata: the resulting serialized graph
json2entity.py: a script to read entries from the json files and put them in the API.ai entities (via REST requests)
