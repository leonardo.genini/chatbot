This folders contains the code of the spider used to scrape http://gameofthrones.wikia.com/wiki/Game_of_Thrones_Wiki

The code was run on a python 2.7.12 virtual environment and the required libraries can be found in the requirements.txt file.

To run the spider, use the command "scrapy crawl GoTChars" from the root folder.

Files of interest are
GoTSpider/spiders/got_spider.py : the main code of the spider
GoTSpider/items.py : containing the code of the extracted items
GoTSpider/pipelines.py : containing the code of the item to json pipeline
GoTSpider/settings.py : various settings including the activation of scrapy's build-in image pipeline
GoTcharacter.json, GoTevent.json, GoThouse.json and GoTplace.json : containing the scraped data
The img folder, containing the scraped images
