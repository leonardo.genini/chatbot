# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class Event(scrapy.Item):
    url = scrapy.Field()
    number = scrapy.Field()
    name = scrapy.Field()
    eventType = scrapy.Field()
    shortDescr = scrapy.Field()
    year = scrapy.Field()
    references = scrapy.Field()
    characterRefs = scrapy.Field()
    houseRefs = scrapy.Field()
    placeRefs = scrapy.Field()
    casualtiesRefs = scrapy.Field()
    battleRefs = scrapy.Field()
    previous = scrapy.Field()
    image_urls = scrapy.Field()
    images = scrapy.Field()

class Character(scrapy.Item):
    number = scrapy.Field()
    url = scrapy.Field()
    name = scrapy.Field()
    appearances = scrapy.Field()
    titles = scrapy.Field()
    aka = scrapy.Field()
    status = scrapy.Field()
    death = scrapy.Field()
    deathRefs = scrapy.Field()
    age = scrapy.Field()
    origin = scrapy.Field()
    originRefs = scrapy.Field()
    allegiance = scrapy.Field()
    allegianceRefs= scrapy.Field()
    father = scrapy.Field()
    fatherRefs = scrapy.Field()
    mother = scrapy.Field()
    motherRefs = scrapy.Field()
    spouse = scrapy.Field()
    spouseRefs = scrapy.Field()
    children = scrapy.Field()
    childrenRefs = scrapy.Field()
    siblings = scrapy.Field()
    siblingsRefs = scrapy.Field()
    lovers = scrapy.Field()
    loversRefs = scrapy.Field()
    shortDescr = scrapy.Field()
    biography = scrapy.Field()
    personality = scrapy.Field()
    relationships = scrapy.Field()
    image_urls = scrapy.Field()
    images = scrapy.Field()

class House(scrapy.Item):
    number = scrapy.Field()
    url = scrapy.Field()
    name = scrapy.Field()
    sigil = scrapy.Field()
    words = scrapy.Field()
    seat = scrapy.Field()
    seatRef = scrapy.Field()
    lord = scrapy.Field()
    lordRef = scrapy.Field()
    allegiance = scrapy.Field()
    allegianceRefs = scrapy.Field()
    vassals = scrapy.Field()
    vassalsRefs = scrapy.Field()
    members = scrapy.Field()
    membersRefs = scrapy.Field()
    shortDescr = scrapy.Field()
    image_urls = scrapy.Field()
    images = scrapy.Field()

class Place(scrapy.Item):
    number = scrapy.Field()
    url = scrapy.Field()
    name = scrapy.Field()
    houseRef = scrapy.Field()
    location = scrapy.Field()
    locationRefs = scrapy.Field()
    shortDescr = scrapy.Field()
    placeType = scrapy.Field()
    image_urls = scrapy.Field()
    images = scrapy.Field()

