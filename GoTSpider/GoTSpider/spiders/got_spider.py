import scrapy

from GoTSpider.items import *

class GoTSpider(scrapy.Spider):
    name = "GoTChars"
    url_header = 'http://gameofthrones.wikia.com'
    eventcount = 0
    charcount = 0
    housecount = 0
    placecount = 0
    allowed_domains = ['gameofthrones.wikia.com']
    start_urls = [
    'http://gameofthrones.wikia.com/wiki/Timeline'
    ]

    def parse(self, response):
        references = []
		
        eventXpaths = response.xpath('//h2[span[@id="Differences_from_the_books"]]/preceding-sibling::ul[preceding-sibling::nav[1]]/li')
        for evt in eventXpaths:
            GoTSpider.eventcount+=1
            event = Event(url=response.url+"_event_"+str(GoTSpider.eventcount))
            event['name'] = "Timeline-event-"+str(GoTSpider.eventcount)
            event['number'] = GoTSpider.eventcount
            event['year'] = evt.xpath('./descendant::b/text()').extract()
            event['shortDescr'] = evt.xpath('./descendant-or-self::*/text()').extract()
            hasReferences = evt.xpath('./descendant::a/@href').extract_first()
            if hasReferences is not None:
                event['references'] = [r for r in evt.xpath('./descendant::a/@href').extract() if r.startswith('/wiki')]
                references.extend(event['references'])
            event['eventType'] = 'Story'
            if GoTSpider.eventcount > 1:
                event['previous'] = response.url+"_event_"+str(GoTSpider.eventcount-1)
            yield event
	
        for ref in references:
            ref_url = response.urljoin(ref)
            yield scrapy.Request(ref_url, callback=self.parseRef)	
	
    def parseRef(self, response):
        tags = set(response.xpath('//div[@class="page-header__categories-links"]//a/text()').extract())
	
        if not(tags.isdisjoint({'Status: Alive', 'Status: Dead', 'Deceased individuals', 'Living individuals'})):
            return self.parseCharacter(response)
        elif not(tags.isdisjoint({'Castles', 'Cities', 'Regions', 'Continents', 'Locations'})):
            return self.parsePlace(response)
        elif not(tags.isdisjoint({'Organizations', 'Noble houses', 'Non-Human Races', 'Peoples'})):
            return self.parseHouse(response)
        elif not(tags.isdisjoint({'Wars','Battles', 'Massacres and Assassinations', 'Events'})):
            return self.parseEvent(response)
        else :
            return

    def parseEvent(self, response):
        refs = []

        event = Event(url=response.url)

        tags = response.xpath('//div[@class="page-header__categories-links"]//a/text()').extract()
        if 'Wars' in tags:
            event['eventType'] = 'War'
        elif 'Massacres and Assassinations' in tags:
            event['eventType'] = 'Assassination'
        elif 'Battles' in tags:
            event['eventType'] = 'Battle'
        elif 'Events' in tags:
            event['eventType'] = 'Story'
        else :
            return

        event['name'] = response.xpath('//h1[@class="page-header__title"]/text()').extract_first()
    
        event['shortDescr'] = ' '.join(response.xpath('//div[@id="mw-content-text"]/nav[@id="toc"]/preceding-sibling::p/descendant-or-self::*/text()').extract())

        GoTSpider.eventcount+=1
        event['number'] = GoTSpider.eventcount

        event['image_urls']=response.xpath('//aside//img[@class="pi-image-thumbnail"]/@src').extract()
        hasPlace = response.xpath('//aside//h3[.="Place"]').extract_first()
        if hasPlace is not None:
            event['placeRefs'] = response.xpath('//aside//div[h3[.="Place"]]//a/@href').extract()
            refs.extend(event['placeRefs'])

	
        event['houseRefs'] = response.xpath('//aside//table[caption[.="Combatants"]]//a/@href').extract()
        refs.extend(event['houseRefs'])

        event['characterRefs'] = [r for r in response.xpath('//aside//table[caption[.="Commanders"]]//a/@href').extract() if r.startswith('/wiki')]
        refs.extend(event['characterRefs'])

        event['casualtiesRefs'] = [r for r in response.xpath('//aside//table[caption[.="Casualties"]]//a/@href').extract() if r.startswith('/wiki')]
        refs.extend(event['casualtiesRefs'])

        event['battleRefs'] = response.xpath('//aside//div[h3[.="Battles"]]//a/@href').extract()
        refs.extend(event['battleRefs'])

        yield event

        for ref in refs:
            ref_url = response.urljoin(ref)
            yield scrapy.Request(ref_url, callback=self.parseRef)
    
    def parseCharacter(self, response):
        charRefs = []
        houseRefs = []
        placeRefs = []

        char = Character(url=response.url)

        char['name'] = response.xpath('//h1[@class="page-header__title"]/text()').extract_first()

        tags = response.xpath('//div[@class="page-header__categories-links"]//a/text()').extract()
        if 'Status: Alive' in tags:
            char['status'] = 'Alive'
        elif 'Living individuals' in tags:
            char['status'] = 'Alive'
        elif 'Status: Dead' in tags:
            char['status'] = 'Dead'
        elif 'Deceased individuals' in tags:
            char['status'] = 'Dead'
        else :
            return

        char['image_urls']=response.xpath('//aside//img[@class="pi-image-thumbnail"]/@src').extract()
    
        hasAppearances = response.xpath('//aside//div[preceding-sibling::h3[1][.="Appeared in"]]/text()').extract_first()
        if hasAppearances is not None:
            nb = [int(s) for s in ' '.join(response.xpath('//aside//div[preceding-sibling::h3[1][.="Appeared in"]]/text()').extract()).split() if s.isdigit()]
            if len(nb) == 0:
                char['appearances'] = 1
            else:
                char['appearances'] = [int(s) for s in ' '.join(response.xpath('//aside//div[preceding-sibling::h3[1][.="Appeared in"]]/text()').extract()).split() if s.isdigit()][0]

		
        hasTitles = response.xpath('//aside//h3[.="Titles"]').extract_first()
        if hasTitles is not None:
            char['titles'] = response.xpath('//aside/div[h3[.="Titles"]]//a/descendant-or-self::*/text()').extract()

        hasAka = response.xpath('//aside//h3[.="Also known as"]').extract_first()
        if hasAka is not None:
            char['aka'] = response.xpath('//aside//div[preceding-sibling::h3[1][.="Also known as"]]/descendant-or-self::*/text()').extract()

        if char['status'] == 'Dead':
            char['death'] = ' '.join(response.xpath('//aside//div[preceding-sibling::h3[1][.="Death"]]/descendant-or-self::*/text()').extract())
            char['deathRefs'] = response.xpath('//aside//div[h3[.="Death"]]//a/@href').extract()


        hasAge = response.xpath('//aside//h3[.="Age"]').extract_first()
        if hasAge is not None:
            char['age'] = [int(s) for s in ' '.join(response.xpath('//aside//div[preceding-sibling::h3[1][.="Age"]]/text()').extract()).split() if s.isdigit()][0]

        hasOrigin = response.xpath('//aside//h3[.="Origin"]').extract_first()
        if hasOrigin is not None:
            char['origin'] = response.xpath('//aside//div[preceding-sibling::h3[1][.="Origin"]]/descendant-or-self::*/text()').extract()
            char['originRefs'] = response.xpath('//aside//div[h3[.="Origin"]]//a/@href').extract()
            placeRefs.extend(char['originRefs'])
            
        hasAllegiance = response.xpath('//aside//h3[.="Allegiance"]').extract_first()
        if hasAllegiance is not None:
            char['allegiance'] = response.xpath('//aside//div[preceding-sibling::h3[1][.="Allegiance"]]/descendant-or-self::*/text()').extract()
            char['allegianceRefs'] = response.xpath('//aside//div[h3[.="Allegiance"]]//a/@href').extract()
            houseRefs.extend(char['allegianceRefs'])		

        hasFather = response.xpath('//aside//h3[.="Father"]').extract_first()
        if hasFather is not None:
            char['father'] = response.xpath('//aside//div[preceding-sibling::h3[1][.="Father"]]/descendant-or-self::*/text()').extract()
            char['fatherRefs'] = response.xpath('//aside//div[h3[.="Father"]]//a/@href').extract()
            charRefs.extend(char['fatherRefs'])

        hasMother = response.xpath('//aside//h3[.="Mother"]').extract_first()
        if hasMother is not None:
            char['mother'] = response.xpath('//aside//div[preceding-sibling::h3[1][.="Mother"]]/descendant-or-self::*/text()').extract()
            char['motherRefs'] = response.xpath('//aside//div[h3[.="Mother"]]//a/@href').extract()
            charRefs.extend(char['motherRefs'])

        hasSpouse = response.xpath('//aside//h3[.="Spouse"]').extract_first()
        if hasSpouse is not None :
            char['spouse'] = response.xpath('//aside//div[preceding-sibling::h3[1][.="Spouse"]]/descendant-or-self::*/text()').extract()
            char['spouseRefs'] = response.xpath('//aside//div[h3[.="Spouse"]]//a/@href').extract()
            charRefs.extend(char['spouseRefs'])

        hasChildren = response.xpath('//aside//h3[.="Children"]').extract_first()
        if hasChildren is not None:
            char['children'] = response.xpath('//aside//div[preceding-sibling::h3[1][.="Children"]]/descendant-or-self::*/text()').extract()
            char['childrenRefs'] = response.xpath('//aside//div[h3[.="Children"]]//a/@href').extract()
            charRefs.extend(char['childrenRefs'])

        hasSiblings = response.xpath('//aside//h3[.="Siblings"]').extract_first()
        if hasSiblings is not None:
            char['siblings'] = response.xpath('//aside//div[preceding-sibling::h3[1][.="Siblings"]]/descendant-or-self::*/text()').extract()
            char['siblingsRefs'] = response.xpath('//aside//div[h3[.="Siblings"]]//a/@href').extract()
            charRefs.extend(char['siblingsRefs'])

        hasLovers = response.xpath('//aside//h3[.="Lovers"]').extract_first()
        if hasLovers is not None:
            char['lovers'] = response.xpath('//aside//div[preceding-sibling::h3[1][.="Lovers"]]/descendant-or-self::*/text()').extract()
            char['loversRefs'] = response.xpath('//aside//div[h3[.="Lovers"]]//a/@href').extract()
            charRefs.extend(char['loversRefs'])

        char['shortDescr'] = ' '.join(response.xpath('//div[@id="mw-content-text"]/nav[@id="toc"]/preceding-sibling::p/descendant-or-self::*/text()').extract())

        bioXpaths = response.xpath('//div[@id="mw-content-text"]//h3[span[contains(@id,"Season")]]/following-sibling::p[preceding-sibling::h2[1][span[@id="Biography"]]]')

        hasBiography = bioXpaths.extract_first()
        if hasBiography is not None: 
            char['biography'] = [' '.join(par.xpath('./descendant-or-self::*/text()').extract()) for par in bioXpaths]

        hasPersonality = response.xpath('//div[@id="mw-content-text"]//h2[span[@id="Personality"]]').extract_first()
        if hasPersonality is not None:
            char['personality'] = ' '.join(response.xpath('//div[@id="mw-content-text"]/p[preceding-sibling::h2[1][span[@id="Personality"]]]/descendant-or-self::*/text()').extract())

        hasRelationships = response.xpath('//div[@id="mw-content-text"]//h2[span[@id="Relationships"]]').extract_first()
        if hasRelationships is not None:
            char['relationships'] = ' '.join(response.xpath('//div[@id="mw-content-text"]/p[preceding-sibling::h2[1][span[@id="Relationships"]]]/descendant-or-self::*/text()').extract())

        GoTSpider.charcount+=1
        char['number'] = GoTSpider.charcount

        yield char

        for ref in charRefs:
            ref_url = response.urljoin(ref)
            yield scrapy.Request(ref_url, callback=self.parseCharacter)

        for ref in houseRefs:
            ref_url = response.urljoin(ref)
            yield scrapy.Request(ref_url, callback=self.parseHouse)	
            
        for ref in placeRefs:
            ref_url = response.urljoin(ref)
            yield scrapy.Request(ref_url, callback=self.parsePlace)	           

    def parseHouse(self, response):
        references = []

        house = House(url=response.url)

        house['name'] = response.xpath('//h1[@class="page-header__title"]/text()').extract_first()
    
        house['image_urls']=response.xpath('//aside//img[@class="pi-image-thumbnail"]/@src').extract()

        hasSigil = response.xpath('//aside//h3[.="Sigil"]').extract_first()
        if hasSigil is not None:
            house['sigil'] = ' '.join(response.xpath('//aside//div[preceding-sibling::h3[1][.="Sigil"]]/descendant-or-self::*/text()').extract())

        hasWords = response.xpath('//aside//h3[.="Words"]').extract_first()
        if hasWords is not None:
            house['words'] = response.xpath('//aside//div[preceding-sibling::h3[1][.="Words"]]/descendant-or-self::*/text()').extract()

        hasSeat = response.xpath('//aside//h3[.="Seat"]').extract_first()
        if hasSeat is not None:
            house['seat'] = response.xpath('//aside//div[preceding-sibling::h3[1][.="Seat"]]/descendant-or-self::*/text()').extract()
            house['seatRef'] = response.xpath('//aside//div[h3[.="Seat"]]//a/@href').extract()
            references.extend(house['seatRef'])


        hasLord = response.xpath('//aside//h3[.="Lord"]').extract_first()
        if hasLord is not None:
            house['lord'] = response.xpath('//aside//div[preceding-sibling::h3[1][.="Lord"]]/descendant-or-self::*/text()').extract()
            house['lordRef'] = response.xpath('//aside//div[h3[.="Lord"]]//a/@href').extract()
            references.extend(house['lordRef'])

        hasAllegiance = response.xpath('//aside//h3[.="Allegiance"]').extract_first()
        if hasAllegiance is not None:
            house['allegiance'] = response.xpath('//aside//div[preceding-sibling::h3[1][.="Allegiance"]]/descendant-or-self::*/text()').extract()
            house['allegianceRefs'] = response.xpath('//aside//div[h3[.="Allegiance"]]//a/@href').extract()
            references.extend(house['allegianceRefs'])

        hasVassals = response.xpath('//aside//h3[.="Vassals"]').extract_first()
        if hasVassals is not None:
            house['vassals'] = response.xpath('//aside//div[preceding-sibling::h3[1][.="Vassals"]]/descendant-or-self::*/text()').extract()
            house['vassalsRefs'] = response.xpath('//aside//div[h3[.="Vassals"]]//a/@href').extract()
            references.extend(house['vassalsRefs'])


        house['shortDescr'] = ' '.join(response.xpath('//div[@id="mw-content-text"]/nav[@id="toc"]/preceding-sibling::p/descendant-or-self::*/text()').extract())

        hasMembers = response.xpath('//h2[span[@id="Family_Tree"]]').extract_first()
        if hasMembers is not None:
            house['members'] = response.xpath('//h2[span[@id="Family_Tree"]]/following-sibling::table[1]/descendant::a[not(. = "Deceased")]/text()').extract()
            house['membersRefs'] = response.xpath('//h2[span[@id="Family_Tree"]]/following-sibling::table[1]/descendant::a[not(. = "Deceased")]/@href').extract()
            references.extend(house['membersRefs'])

        GoTSpider.housecount+=1
        house['number'] = GoTSpider.housecount

        yield house

        for ref in references:
            ref_url = response.urljoin(ref)
            yield scrapy.Request(ref_url, callback=self.parseRef)
		
    
    def parsePlace(self, response):
        houseRefs = []
        place = Place(url=response.url)

        place['image_urls']=response.xpath('//aside//img[@class="pi-image-thumbnail"]/@src').extract()    
    
        place['name'] = response.xpath('//h1[@class="page-header__title"]/text()').extract_first()

        hasHouse = response.xpath('//aside//h3[.="Rulers"]').extract_first()
        if hasHouse is not None:
            place['houseRef'] = response.xpath('//aside//div[h3[.="Rulers"]]//a/@href').extract()
            houseRefs.extend(place['houseRef'])

        place['location'] = ' '.join(response.xpath('//aside//div[preceding-sibling::h3[1][.="Location"]]/descendant-or-self::*/text()').extract())
        place['locationRefs'] = response.xpath('//aside//div[preceding-sibling::h3[1][.="Location"]]//a/@href').extract()
    
        place['shortDescr'] = ' '.join(response.xpath('//div[@id="mw-content-text"]/nav[@id="toc"]/preceding-sibling::p/descendant-or-self::*/text()').extract())	

        tags = response.xpath('//div[@class="page-header__categories-links"]//a/text()').extract()
        if 'Castles' in tags:
            place['placeType'] = 'Castle'
        elif 'Cities' in tags:
            place['placeType'] = 'City'
        elif 'Regions' in tags:
            place['placeType'] = 'Region'
        elif 'Continents' in tags:
            place['placeType'] = 'Continent'
        else :
            return
	
        GoTSpider.placecount+=1
        place['number'] = GoTSpider.placecount

        yield place	

        for ref in houseRefs:
            ref_url = response.urljoin(ref)
            yield scrapy.Request(ref_url, callback=self.parseHouse)