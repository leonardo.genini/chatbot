# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.xlib.pydispatch import dispatcher 
from scrapy import signals
from scrapy.exporters import JsonItemExporter

def item_type(item):
    return type(item).__name__.lower()

#inspired from https://stackoverflow.com/questions/12230332/how-can-scrapy-export-items-to-separate-csv-files-per-item
class JsonItemExportPipeline(object):
    itemTypes = ['character','house','event', 'place']
    files = dict()
    exporters = dict()

    def __init__(self):
        dispatcher.connect(self.spider_opened, signal=signals.spider_opened)
        dispatcher.connect(self.spider_closed, signal=signals.spider_closed)

    def spider_opened(self, spider):
        self.files = dict([ (name, open('GoT'+name+'.json','w+b')) for name in self.itemTypes ])
        self.exporters = dict([ (name,JsonItemExporter(self.files[name], indent = 1)) for name in self.itemTypes])
        [e.start_exporting() for e in self.exporters.values()]

    def spider_closed(self, spider):
        [e.finish_exporting() for e in self.exporters.values()]
        [f.close() for f in self.files.values()]

    def process_item(self, item, spider):
        what = item_type(item)
        if what in set(self.itemTypes):
            self.exporters[what].export_item(item)
        return item
